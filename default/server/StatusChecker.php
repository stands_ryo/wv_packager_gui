<?php


require_once('../conf/inc.php');

$data=$_GET;
$job_id=$data["job_id"];

//initialize curl session
$ch=curl_init();
curl_setopt($ch, CURLOPT_URL, $packager_status_url."?job_id=".$job_id);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_USERPWD, $id.":".$pw);
curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_NOPROGRESS, FALSE);
    
// execute curl session
$response=curl_exec($ch);
$info=curl_getinfo($ch);
$http_code=$info["http_code"];
if ($http_code == 200)  //response retrieved
{
    //$response_array=json_decode($response, TRUE);
    header("HTTP/1.0 200 OK");
}
else if ($http_code == 400){  //jobid not found
    header("HTTP/1.0 400 Bad Request");
    echo $response;
}
else if ($http_code == 401){  //not authorized
    header("HTTP/1.0 401 Unauthorized");
}
else  //status unknown
    header("HTTP/1.0 500 Internal Server Error");
   
//close handle
curl_close($ch);

echo $response;
exit();


?>
