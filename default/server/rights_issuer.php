<?php
// rights_issuer.php sample script coded by Kenji Saito
// no authentication/error handling implemented
// it is not recommended to use this script as is

require_once('../conf/inc.php');
// get request parameters
$data=$_POST;

// send request to license proxy
$content=http_build_query($data);
$header=implode("\r\n", array("Content-Type: application/x-www-form-urlencoded", "Content-Length: ".strlen($content)));
$options=stream_context_create(array("http" => array("method" => "POST", "header" => $header, "content" => $content)));
$socket_timeout=10;
ini_set("default_socket_timeout", $socket_timeout);
$response=file_get_contents($license_proxy_url."?serverid=skillup&duration=86400&fixdevice=0", false, $options);

// send response back
echo $response;
?>
