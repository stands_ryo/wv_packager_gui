<?php


require_once('../conf/inc.php');


function validate_temp($target, $accept_characters, $accept_length){

    $message='';

    switch ($target){
        case "job_id":
            $accept_characters = "/^[a-zA-Z0-9-\._]+$/";
            $accept_length = 50;
            if (!preg_match($accept_characters, $target)) {
                $message = 'Job Idに利用できない文字が含まれています。';
                break;
            }
            if (mb_strlen($target) > $accept_length) {
                $message = 'Job Idは50文字以内で入力してください。';
                break;
            }
            break;
        case "policy":
            $accept_characters = "/^[a-zA-Z0-9-\._]+$/";
            $accept_length = 50;
            if (!preg_match($accept_characters, $target)) {
                $message = '暗号化ポリシーに利用できない文字が含まれています。';
                break;
            }
            if (mb_strlen($target) > $accept_length) {
                $message = '暗号化ポリシーは50文字以内で入力してください。';
                break;
            }
            break;
        case "output_file_name":
            $accept_characters = "/^[a-zA-Z0-9-\._]+$/";
            $accept_length = 50;
            if (!preg_match($accept_characters, $target)) {
                $message = '暗号化後ファイル名に利用できない文字が含まれています。';
                break;
            }
            if (mb_strlen($target) > $accept_length) {
                $message = '暗号化後ファイル名は50文字以内で入力してください。';
                break;
            }
            break;
        case "destination":
            $accept_characters = "/^[a-zA-Z0-9-\._\/]+$/";
            $accept_length = 100;
            if (!preg_match($accept_characters, $target)) {
                $message = '暗号化後ファイルのアップロードURLに利用できない文字が含まれています。';
                break;
            }
            if (mb_strlen($target) > $accept_length) {
                $message = '暗号化後ファイルのアップロードURLは50文字以内で入力してください。';
                break;
            }
            break;
        default:
            $message= '不明なエラーです。';
            break;
    }
    

    return $message;
}



$accept_characters = "/^[a-zA-Z0-9-\._]+$/";
$accept_length = 50;

// requested parameters
$data=$_GET;
$job_id=(isset($data["job_id"]) ? $data["job_id"] : "");
$output_file_name=(isset($data["output_file_name"]) ? $data["output_file_name"] : "");
$policy=(isset($data["policy"]) ? $data["policy"] : "");
$destination=(isset($data["destination"]) ? $data["destination"] : "");
$enctype=(isset($data["enctype"]) ? $data["enctype"] : "");
$sources=array();
for ($i=0; $i < 10; $i++)
{
    if (isset($data["source$i"]) && $data["source$i"])
        $sources[$i]=$data["source$i"];
    else
        break;
}
$num_files=$i;
$error=(isset($data["error"]) ? $data["error"] : "");



// validate job_id 
if (!preg_match($accept_characters, $job_id)) {
    $error="2";
}
if (mb_strlen($job_id) > $accept_length) {
    $error="3";
}

// validate enctype
if(strcmp($enctype, "mp4") != 0) $enctype="wvm";

// if upload error occured , remove files
if($error){
    for ($i=0; $i < $num_files; $i++){
        unlink($uploaded_dir.$sources[$i]);
    }

    $response=array(
        'status' => 'error', 
        'job_id' => $job_id
    );

    switch($error){
    case 1: 
        $response +=array( 'message' => 'mp4ファイルを確認して下さい。');
        break;
    case 2: 
        $response +=array( 'message' => 'Job Idに利用できない文字が含まれています。');
        break;
    case 3: 
        $response +=array( 'message' => 'Job Id は50文字以内で入力してください。');
        break;
    }
    echo json_encode($response);
    exit();
}

// generate json
$video_array=array();
for ($i=0; $i < $num_files; $i++)
        $video_array["video".($i+1)]=$sources[$i];

$json_array=array();
$json_array["video"]=$video_array;
//$json_array["output_file_name"]=$job_id.".".$enctype;

if(!empty($output_file_name)){
    $json_array["output_file_name"]=$output_file_name;
}
if(!empty($policy)){
    $json_array["policy"]=$policy;
}
if(!empty($destination)){
    $json_array["destination"]=$destination;
}

$json=json_encode($json_array, JSON_UNESCAPED_SLASHES);
$json_name = $job_id.'.json';


$fh=fopen($uploaded_dir.$json_name, "w");
fwrite($fh, $json);
fclose($fh);


// move source files
for ($i=0; $i < $num_files; $i++){
    if(!rename($uploaded_dir.$sources[$i] ,$initiator_inbox.$sources[$i])){
        $response=array(
            'status' => 'error', 
            'job_id' => $job_id,
            'message' => 'Please try again later.'
        );
        //response
        echo json_encode($response);
        exit();
    }
}
if(!rename($uploaded_dir.$json_name ,$initiator_inbox.$json_name)){
    $response=array(
        'status' => 'error', 
        'job_id' => $job_id,
        'message' => 'Please try again later.'
    );
    //response
    echo json_encode($response);
    exit();
}

//success
$response=array(
    'status' => 'ok', 
    'job_id' => $job_id
);

// response
echo json_encode($response);
exit();


?>
