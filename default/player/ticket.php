<?php
// constants
$ticket_url_base="https://wvlp01.uliza.jp/lp/customer01/generate_ticket.php";
$server_id="skillup";
$policy="default";
$duration=86400;
$fixdevice=0;

// parameters
$videoid=(isset($_GET["videoid"]) ? $_GET["videoid"] : "default_video_id");

// client authentication and authorization
$authorized=0;
//------------
// here client should be authenticated and its entitlement should be validated
// example)
//  authenticate client with session or something
//  validate its entitlement in cooperation with CMS/SMS
//  set $authorized=1 if client is authorized
//  record some logs if needed
$authorized=1;
//------------

// get ticket
if ($authorized)
{
 $ticket_url=$ticket_url_base."?serverid=".$server_id."&streamid=".$videoid."&policy=".$policy."&duration=".$duration."&fixdevice=".$fixdevice;
 $ticket=file_get_contents($ticket_url);
 echo $ticket;
}
?>