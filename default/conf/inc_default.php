<?php

$license_proxy_url="%LICENSE_PROXY_URL%";
$packager_status_url='%PACKAGER_STATUS_URL%';
$id="%ACCOUNT_ID%";
$pw="%PASSWORD%";


$initiator_inbox='%I_INBOX_DIR%';
$uploaded_dir='%UPLOADED_DIR%';

function oreno_log($message)
{
    date_default_timezone_set('Asia/Tokyo');
    $timestamp=time();
    $timestamp_m=date("Y/m/d H:i:s", $timestamp);
    $timestamp_f=date("Ymd", $timestamp);
    error_log("[${timestamp_m}] ${message}\n", 3, "server/logs/php.log");
}

?>
