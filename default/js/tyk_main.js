/*
 * Widevine Packager Demo
 *
 * Copyright 2013, Skillup video technologies
 * http://suvt.co.jp/
 *
 */

/* ajax function for watching job status */
$(function () {
    var timer;

    var Job = {
        check: function(query) {
        var defer = $.Deferred();
        $.ajax({
            url: "server/StatusChecker.php",
            data: {
              job_id: document.getElementById('ck_job_id').value
            },
            dataType: 'json',
            success: defer.resolve,
            error: defer.reject
        });
        
        return defer.promise();
      }
    };


    $('#form_jobcheck').submit(function(event) {
      event.preventDefault();
      clearInterval(timer);

      showStatusResult();

      // watch status for every 10 seconds
      timer = setInterval(function() {showStatusResult(); },10000);
    });

    function showStatusResult() {

      if(document.getElementById("ck_job_id").value == ""){
        return;
      }

      $('#btn_job_check').attr('disabled',true);
      document.getElementById('p_load_job').style.display = "";

      Job.check('jquery deferred').done(function(data) {
        console.log(data);
        switch (data.status){
          case "0":
            document.getElementById('p_status').innerHTML = 'パッケージ処理待ち';
            document.getElementById('dv_progressbar').className = "progress-bar progress-bar-warning";
            document.getElementById('dv_progressbar').setAttribute('style','width:25%;');
            break;
          case "10":
            document.getElementById('p_status').innerHTML = 'パッケージ処理中';
            document.getElementById('dv_progressbar').className = "progress-bar progress-success";
            document.getElementById('dv_progressbar').setAttribute('style','width:50%;');
            break;
          case "11":
            document.getElementById('p_status').innerHTML = 'パッケージ後ファイルを転送中';
            document.getElementById('dv_progressbar').className = "progress-bar progress-bar";
            document.getElementById('dv_progressbar').setAttribute('style','width:75%;');
            break;
          case "1":
            document.getElementById('p_status').innerHTML = '完了';
            document.getElementById('dv_progressbar').className = "progress-bar progress-bar";
            document.getElementById('dv_progressbar').setAttribute('style','width:100%;');
            break;
          case "500":
            document.getElementById('alert_fail_job').innerHTML = '<strong>パッケージジョブ処理中にエラーが発生しました。</strong>';
            break;
        }

        switch (data.status){
          case "500":
            if(document.getElementById('alert_fail_job').style.display == "none"){
              document.getElementById('alert_fail_job').style.display = "";
              document.getElementById('dv_status').style.display = "none";
            }
            break;
          default:
            if(document.getElementById('dv_status').style.display == "none"){
              document.getElementById('dv_status').style.display = "";
              document.getElementById('alert_fail_job').style.display = "none";
            }
            break;
        }
        document.getElementById('p_load_job').style.display = "none";
        $('#btn_job_check').attr('disabled',false);


      }).fail(function(jqXHR,data) {
        console.log(data);
        console.log(jqXHR.status);


        switch(jqXHR.status){
          case 400:
            document.getElementById('alert_fail_job').innerHTML = '<strong>指定されたパッケージジョブが見つかりません。</strong>';
            break;
          default : 
            document.getElementById('alert_fail_job').innerHTML = '<strong>時間をおいてやり直してください。</strong>';
            break;
        }

        document.getElementById('dv_status').style.display = "none";
        document.getElementById('p_load_job').style.display = "none";
        document.getElementById('alert_fail_job').style.display = "";
        $('#btn_job_check').attr('disabled',false);

      });

    }

});


/* function for preview */
$(function () {

    $('#form_preview').submit(function(event) {
      event.preventDefault();
      $('#btn_preview').attr('disabled',true);
      
      document.getElementById('p_load_preview').style.display = "";

      // set player
      unsetPlayer('wvplayer');
      if(document.getElementById('pv_job_id').value != ''){
        setPlayer('playerContainer', 'wvplayer', document.getElementById('pv_job_id').value, videoid_);
      }

      document.getElementById('p_load_preview').style.display = "none";
      $('#btn_preview').attr('disabled',false);

    });
});

