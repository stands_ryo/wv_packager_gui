var playerObjectName;


// check if wv plugin installed
var plugin_version=null;
function pluginInstalled()
{
 var plugin;
 plugin=document.getElementById('WidevinePlugin');
 if (plugin)
 {
  try
  {
   plugin_version=plugin.GetVersion();
   return true;
  }
  catch (e)
  {
   return false;
  }
 }
 else
  return false;
}

// get ticket
function getTicket(videoid)
{
  var xhr;
  try
  {
    xhr=new XMLHttpRequest();
  }
  catch(e)
  {
    try
  {
    xhr=new ActiveXObject("Msxml2.XMLHTTP");
  }
  catch(e)
  {
    xhr=new ActiveXObject("Microsoft.XMLHTTP");
  }
}
 var ticketUrl="player/ticket.php?videoid="+videoid;
 xhr.open("GET", ticketUrl, false);
 xhr.send(null);
 return xhr.responseText;
}

/*
// set player
function setPlayer(containerID, playerID, url, videoid,online)
{
  if(!pluginInstalled())
    return;
  //widevine設定
  WVSetStreamId(videoid);
  WVSetEmmURL(emmUrl);
  WVSetPortal(portal);

  playerObjectName = playerID;
  var element=document.createElement("div"); 
  element.id=playerID; 
  document.getElementById(containerID).appendChild(element);
  var flashVars={
    src: url,
    adaptiveBitrate: 0,
    loop:true,
    autoPlay:true
  };
  var params={
    allowScriptAccess: "always",
    allowFullScreen: "true",
    wmode: "opaque"
  };
  var attributes={
    id: playerID,
    name: playerID
  };
  swfobject.embedSWF("WidevinePlayer.swf", playerID, 640, 360, "11.2.0", {}, flashVars, params, attributes);
}
*/
// set player
function setPlayer(containerID, playerID, url, videoid,online)
{
  if(!pluginInstalled())
    return;
  //widevine設定
  WVSetStreamId(videoid);
  WVSetEmmURL(emmUrl);
  WVSetPortal(portal);

  playerObjectName = playerID;
  var element=document.createElement("div"); 
  element.id=playerID; 
  document.getElementById(containerID).appendChild(element);
  var flashVars={
    //src: 'http://54.248.249.96/tyk/portal/contents/'+ job_id +'.wvm',
    src: url,
    adaptiveBitrate: 0,
    loop:true,
    autoPlay:true
  };
  var params={
    allowScriptAccess: "always",
    allowFullScreen: "true",
    wmode: "opaque"
  };
  var attributes={
    id: playerID,
    name: playerID
  };
  swfobject.embedSWF("player/WidevinePlayer.swf", playerID, 640, 360, "11.2.0", {}, flashVars, params, attributes);
}
// unset player
function unsetPlayer(playerID)
{
 swfobject.removeSWF(playerID);
}

/*
var statuscode_=null;
// status callback from swf
function wvJSEventCallback(event,arg1,arg2)
{
  var pos;
  if("onReady" == event){
    WVSetTicket(getTicket(videoid_));
  }
  if("onPlay" == event){
    pos = swfobject.getObjectById(playerObjectName).getPosition();
    sendBeacon(pos, "play");
    beaconSecCount=0;
  }
  if("onPause" == event){
    pos = swfobject.getObjectById(playerObjectName).getPosition();
    sendBeacon(pos, "pause");
    beaconSecCount=0;
  }
  if("onSeek" == event){
    sendBeacon(arg2,"seek");
    beaconSecCount=0;
  }
  if("onComplete" == event){
    pos = swfobject.getObjectById(playerObjectName).getPosition();
    sendBeacon(pos, "stop");
    beaconSecCount=0;
  }
  if("onTime" == event){
    beaconSecCount++;
    if(beaconSecCount >= 15){
      beaconSecCount = 0;
      sendBeacon(arg1,"playing");
    }
  }
}
*/
// get session id
function getSession()
{
 var cookie_array=String(document.cookie).replace(/\s/g,"").split(";");
 for (var i=0; i < cookie_array.length; i++)
 {
  var cookie=cookie_array[i].split("=");
  if (cookie[0] === "sid")
   return decodeURIComponent(cookie[1]);
 }
 return null;
}

/*
// send beacon
function sendBeacon(pos, event)
{
 var xmlHttpRequest;
 try
 {
  xmlHttpRequest=new XMLHttpRequest();
 }
 catch(e)
 {
  try
  {
   xmlHttpRequest=new ActiveXObject("Msxml2.XMLHTTP");
  }
  catch(e)
  {
   xmlHttpRequest=new ActiveXObject("Microsoft.XMLHTTP");
  }
 }
 xmlHttpRequest.onreadystatechange=function(){
 if (this.readyState == 4) // HTTP_REQUEST_READYSTATE_COMPLETED
  statuscode_=this.status;
 }

 var str=beaconUrl+"?sid="+getSession()+"&videoid="+videoid_+"&pos="+pos+"&event="+event;
 xmlHttpRequest.open("GET", str, true);
 xmlHttpRequest.send(null);
}
*/
function getPlayerObject(){
    return swfobject.getObjectById(playerObjectName);
}
